# Continuation-passing with Style

This repository contains the source of a talk I gave
about **continuations** and **continuation-passing style** (CPS)
in the realm of the Clojure language

## Sources of the talk

The talk is simply a Clojure namespace, with added comments
intended to be shown using the **live-code-talks** Emacs mode.




## License

Copyright © 2021 Frederic Peschjanski

This document and the accompanying materials are made available under the
terms of the Creative Commons  CC-BY-SA 4.0  License  (see LICENSE)
