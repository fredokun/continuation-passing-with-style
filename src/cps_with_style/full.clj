
(ns cps-with-style.full)

;;;  # CPS : Continuation Passing with Style

;;;                  ____                    
;;;             ____ \__ \
;;;             \__ \__/ / __
;;;             __/ ____ \ \ \    ____
;;;            / __ \__ \ \/ / __ \__ \        A Clojure Talk by ...
;;;       ____ \ \ \__/ / __ \/ / __/ / __
;;;  ____ \__ \ \/ ____ \/ / __/ / __ \ \ \
;;;  \__ \__/ / __ \__ \__/ / __ \ \ \ \/          Frederic Peschanski
;;;  __/ ____ \ \ \__/ ____ \ \ \ \/ / __
;;; / __ \__ \ \/ ____ \__ \ \/ / __ \/ /
;;; \ \ \__/ / __ \__ \__/ / __ \ \ \__/                Sorbonne University
;;;  \/ ____ \/ / __/ ____ \ \ \ \/ ____
;;;     \__ \__/ / __ \__ \ \/ / __ \__ \
;;;     __/ ____ \ \ \__/ / __ \/ / __/ / __     (github: fredokun)
;;;    / __ \__ \ \/ ____ \/ / __/ / __ \/ /     (frederic.peschanski@lip6.fr)
;;;    \/ / __/ / __ \__ \__/ / __ \/ / __/
;;;    __/ / __ \ \ \__/ ____ \ \ \__/ / __
;;;   / __ \ \ \ \/ ____ \__ \ \/ ____ \/ /
;;;   \ \ \ \/ / __ \__ \__/ / __ \__ \__/
;;;    \/ / __ \/ / __/ ____ \ \ \__/
;;;       \ \ \__/ / __ \__ \ \/
;;;        \/      \ \ \__/ / __
;;;                 \/ ____ \/ /
;;;                    \__ \__/
;;;                    __/



;;; # Due Credits

;;; The idea of the talk (except the last part) is largely inspired 
;;; by an old french book  (circa 1996)

;;;     __...--~~~~~-._   _.-~~~~~--...__
;;;     //               `V'               \\ 
;;;    //                 |                 \\ 
;;;   //__...--~~~~~~-._  |  _.-~~~~~~--...__\\ 
;;;  //__.....----~~~~._\ | /_.~~~~----.....__\\
;;; ====================\\|//====================
;;;                 dwb `---`

;;; ## Programer avec Scheme 
;;; by Jacques Chazarin... 

;;; It's not printed anymore
;;; and there are only dead-tree versions... but it's one of the best 
;;; programming book I know (and I know SICP, PAIP, Joy of Clojure, PCL, ...)



;;; # Summary 

;;; ## 1. Continuation-passing style : what and how ?

;;; ## 2. CPS in practice : multiple values  and  early aborts

;;; ## 3. The not-so-good parts of CPS

;;; ## 4. Towards Data-driven continuation passing style

;;; ## 5. What's next ?


;;; ### My assumptions

;;; - you're Clojure developer, or aspiring Clojure developer
;;; (thus you know the basics)

;;; - you don't know much about Lisp history in general, and CPS in particular
;;; (especially if you know Scheme already, you'll get bored)




;;; # CPS : What ?

;;; A Continuation is a one-argument function that
;;; tells "what to do next" in a computation.

;;; Callers expecting a return value are implicit continuations...
;;; We call this the (standard) direct style

;;;  ##  Direct style, an example

(defn fact [n]
  (if (zero? n)
    1
    (* n (fact (dec n)))))  
    ;; #(* n %)  is the continuation of (fact (dec n))

(fact 5)

(- (fact 5) 100) 
;; #(- % 100) is the continuation of (fact 5)


;;; Continuation-passing style (CPS) is about making
;;; continuations explicit.

;;; ## The same example, CPS style

(defn fakt [n k]
  (if (zero? n)
    (k 1)
    (recur (dec n) #(k (* n %)))))

;; Hence we have to explain "what to do?"
;; after the computation
(fakt 5 identity)

(fakt 5 #(- % 100))

;;; Remark : the CPS version is tail-call recursive
;;; However its stacks-up the continuations k so there's no real gain
;;; (sometimes there is, e.g. in some cases of deep non-tail recursions...)



;;; # Exercise one : of course Fibonacci !

;;;              ,\
;;;              \\\,_
;;;               \` ,\
;;;          __,.-" =__)
;;;        ."        )
;;;     ,_/   ,    \/\_
;;;     \_|    )_-\ \_-`
;;; jgs    `-----` `--`


(defn fibonacci [n]
  (cond 
    (zero? n) 0
    (= n 1) 1
    :else
    (+ (fibonacci (- n 2)) (fibonacci (- n 1)))))

(fibonacci 8)
(fibonacci 20)
(fibonacci 30)
;; careful beyond that ... 

;; Step 1 : write  a tail-recursive version (with recur of course)

;; Step 2 : write a CPS-style version
;; hint : use a two-arguments continuation
(defn fibonakki [n k]
  (cond
    (zero? n) (k 0 0)
    (= n 1) (k 0 1)
    :else
    (recur (- n 1) #(k %2 (+ %2 %1)))))

(fibonakki 8 (fn [a b] b))
(fibonakki 20 (fn [a b] b))
;; no problem :
(fibonakki 50 (fn [a b] b))



;;; # Is this useful ?

;;; There's no definite answer ... I would say intellectually YES, no doubt !
;;; In practice, there are interesting use cases... Let's see two of the simplest ones

;;; # Case study 1 : multiple values

;;; It many situations we have to handle multiple values
;;; Some languages support this directly (Scheme, Common Lisp, Lua, ...)
;;; Others, like clojure, use tuples or vectors, which can be a little
;;; bit cumbersome

(defn quo&rem [a b]
  (if (< a b)
    [0 a]
    (let [[q r] (quo&rem (- a b) b)]
      [(inc q) r])))

(quo&rem 64 2)

(quo&rem 267 21)

;;; In CPS style :

(defn kuo&rem [a b k]
  (if (< a b)
    (k 0 a)
    (recur (- a b) b #(k (inc %1) %2))))

;;; Remark : here it's a two-arguments continuation (for 2-values computations)

;;; How do we call this function ?

(kuo&rem 64 2 vector)  ;; this could be a two-arity version, by default

(kuo&rem 267 21 vector)

;; Maybe we want a list ?
(kuo&rem 267 21 list)

;; Maybe we want to build a map ?
(kuo&rem 267 21 (fn [q r] {:quotient q, :remainder r}))

;; Maybe we just want the remainder ?
(kuo&rem 267 21 (fn [_ r] r))



;;; # Case study 2 : early aborts

;;; Explicit continuations allow to abort computations early on.
;;; For this we have to separate the computation in two cases:
;;; - a success continuation if the computations goes through
;;; - a fail continuation in case of an abort

;;; ## Example : product of a sequence of numbers

;;; Direct style :
(defn product [s]
  (if (seq s)
    (* (first s) (product (rest s)))
    1))

(product [1 2 3 4 5])

;;; Of course, this is not a good Clojure version, it's for
;;; demonstration purpose

;;; CPS style :
(defn produkt [s k]
  (if (seq s)
    (recur (rest s) #(k (* (first s) %)))
    (k 1)))


(produkt [1 2 3 4 5] identity)

;;; Now suppose we want to abort the computation
;;; if there's a 0 somewhere

;;; Direct style :
(defn product [s]
  (if (seq s)
    (if (zero? (first s))
      0
      (* (first s) (product (rest s))))
    1))

(product (concat (range 1 4199) [6 5 0 3 2]))

;;; CPS style, take one

(defn produkt [s k]
  (if (seq s)
    (if (zero? (first s))
      (k 0)
      (recur (rest s) #(k (* (first s) %))))
    (k 1)))

(produkt (concat (range 1 7349) [6 5 0 3 2]) identity)

;;; CPS style, take two

(defn produkt [s kcont kabort]  ;; a.k.a. ksuccess, kfail
  (if (seq s)
    (if (zero? (first s))
      (kabort 0)
      (recur (rest s) #(kcont (* (first s) %)) kabort))
    (kcont 1)))

(produkt (concat (range 1 100000) [6 5 0 3 2]) identity identity)



;;; # Exercise two : nested product

;;; This is a variant of the product that nests within sub-sequences
(defn product* [s]
  (if (seq s)
    (if (seqable? (first s))
      (* (product* (first s)) (product* (rest s)))
      (* (first s) (product* (rest s))))
    1))

(product* [1 [2 3 [4 5] 6] [7 8] 9 10])

;;; ## Write produkt*,  with CPS and the 0 "abort"
;;; (hint: not an easy one but it's not that far from `produkt`)

;;; ## Solution 

(defn produkt* [s kcont kabort]
  (if (seq s)
    (cond 
      (seqable? (first s))
      (recur (rest s) #(kcont (produkt* (first s) (fn [x] (* x %)) kabort)) kabort)
      
      (= (first s) 0) (kabort 1)
      :else (recur (rest s) #(kcont (* (first s) %)) kabort))
    (kcont 1)))

(produkt* [1 [2 3 [4 5] 6] [7 8] 9 10] identity identity)



;;; # The main drawback of CPS style => Opacity

;;; Explicit continuations are powerfull because they allow
;;; to "play" with the control-flow... We've only scratched the surface.

;;; However, there's one important drawback : they are functions
;;; and are thus opaque : we do not know what's inside.

;;; This causes issues while e.g. debugging

;;; This is a general issue when using functions as data ...
;;; Especially in Clojure, a language in which we love to see
;;; and play with structured, immutable data.

;;; ## Case study : an interactive problem-solver with continuation capture
;;; (disclaimer : this version can be considered ugly by Clojure standards,
;;;  but this is classical Scheme...)

(defn mk-solver
  "Creates a solver that finds the next element of sequence `s` satisfying predicate `p?`.
  Calls `kfail` in case there's no solution."
  [s p? kfail]
  (let [;; the success continuation, as an atom
        ksuccess (atom nil)
        ;; the search procedure
        search (fn search [s k]
                 (cond
                   ;; first case : found a solution
                   (p? s) (do (swap! ksuccess (fn [_] k)) ; capture the continuation 
                                                          ; (for further solutions)
                              s)
                   ;; second case : we search deeper in the sequence
                   (and (sequential? s) (seq s))
                   (recur (first s) (fn [] (search (rest s) k)))
                   :else
                   (k)))]
    (swap! ksuccess (fn [_] (fn [] (search s kfail))))
    (fn [] (@ksuccess))))

(def solve-even (mk-solver [2 3 [5 4 3] 11 9 [9 8]] 
                           #(and (number? %)
                                 (even? %))  
                           (fn [] ::no-more-solution)))

(solve-even)

(defn decrease?
  ([v] (and (sequential? v)
            (seq v)
            (number? (first v))
            (decrease? (rest v) (first v))))
  ([v n] (if (seq v)
           (and (number? (first v))
                (< (first v) n)
                (recur (rest v) (first v)))
           true)))

(def solve-decr (mk-solver [2 3 [5 4 3] 11 9 [9 8]]
                           decrease?
                           (fn [] ::no-more-solution)))


(solve-decr)

;;; This is working, and somewhat powerfull ... However
;;; - the continuations interplay is ... complex
;;; - there are side-effects, hence it's not idiomatic at all in Clojure
;;; - trust me, it's very hard to debug, mostly because everything is opaque
;;;    (functions, atoms)

;;; ## Is there a solution ? 



;;; # Data-oriented CPS style
;;; (disclaimer : this is a very personnal take on the problem)

;;; ## The idea
;;; Instead of passing functions, we'll pass, and return a contextual information
;;; about the control-flow.

(defn solve
  ([p? s] (solve {:predicate p?
                  :todo s}))
  ([ctx]
   (let [s (:todo ctx)
         p? (:predicate ctx)]
     (cond
       ;; first case : the todo is the solution
       (p? s) [s (assoc ctx :todo [])]
       ;; second case : this is actually a non-empty sequence
       (and (sequential? s)
            (seq s)) 
       (cond
         ;; maybe the first element is a solution
         (p? (first s)) 
         [(first s) (assoc ctx :todo (rest s))]
         ;; or it's a non-empty sequence so we look in to it
         (and (sequential? (first s))
              (seq (first s)))
         (recur (assoc ctx :todo (lazy-cat (first s) (rest s))))
         :else ;; we drop the first
         (recur (assoc ctx :todo (rest s))))
       :else ;; third case : no more solution
       nil))))


(def sol1 (solve #(and (number? %)
                        (even? %))
                  [2 3 [5 4 3] 11 9 [9 8]]))

sol1

(def sol2 (solve (second sol1)))

sol2

(def sol3 (solve (second sol2)))

sol3

(solve (second sol3)) ;; no more solution

(defn solve-all [p? s]
  (loop [sol (solve {:predicate p?, :todo s}), res []]
      (if sol
        (recur (solve (second sol)) (conj res (first sol)))
        res)))

(solve-all #(and (number? %)
                 (even? %))
           [2 3 [5 4 3] 11 9 [9 8]])

(solve-all decrease? [2 3 [5 4 3] 11 9 [9 8]])

(defn solve-seq 
  ([p? s] (solve-seq {:predicate p?, :todo s}))
  ([ctx] (when-let [sol (solve ctx)]
           (lazy-seq (cons (first sol) (solve-seq (second sol)))))))

(solve-seq #(and (number? %)
                 (even? %))
           [2 3 [5 4 3] 11 9 [9 8]])

(solve-seq decrease? [2 3 [5 4 3] 11 9 [9 8]])

;;; works for infinite solutions 
;;; (but not absence of solution in infinite sequence !)
(take 10 (solve-seq #(and (number? %)
                          (even? %))
                    (range)))
 



;;; # Exercise three : a "better" mk-solver

;;; We want to rebuild the mk-solver example but
;;; without passing continuations

;;; Hint : use solve-seq













;;; ## Solution


(defn mk-solver [s p?]
  (let [solutions (atom (solve-seq p? s))]
    (fn []
      (if (seq @solutions) (let [s @solutions] 
                           (swap! solutions rest)
                           (first s))
          ::no-more-solution))))

(def solve-even (mk-solver [2 3 [5 4 3] 11 9 [9 8]] 
                           #(and (number? %)
                                 (even? %))))

(solve-even)

(def solve-decr (mk-solver [2 3 [5 4 3] 11 9 [9 8]]
                           decrease?))


(solve-decr)



;;; # Summary

;;; Continuations and CPS provide an insightful view over computations
;;; Separate "What to do now ?" from "What to do next ?"

;;; True CPS can be used, sometimes profitably, in Clojure

;;; However, functions (and atoms, etc.) are opaque
;;; - when appearing as values in data, the data become unintelligible
;;; - prevent testing, debugging (serialization, ...)
;;; - prevent modularity at the control-level, the callee decides when
;;;   to invoke the continuation (a.k.a. "callback hell").

;;; Hence, I advocate the switch from `fn` continuations to
;;; data-oriented control-contexts   (that I often name `cont` or `ctrl` or `ctx`)
;;; that can be passed around

;;; This gives back all good properties : control contexts as map are 
;;; readable/debuggable, and allow "replayability"
;;; the control is modular since the decision about what to do with the control
;;; can be extracted from the functions that use the context, no "callback hell"

;;; Not shown in the talk, but it's also a good starting point to implement
;;; deep recursion without consuming the stack 
;;; (cf. my talk at ClojureD 2018 => https://www.youtube.com/watch?v=NvDOUBQvQ8A)

;;; also, trampolining, etc...



;;; # More about continuations and CPS (with pointers)
;;; (or maybe a "CPS with style next" ?)

;;; ## Continuation monad
;;; How to combine continuations ?

;;; ## Control operators
;;; call/cc , shift/reset and friends ...

;;; ## 



;; Local Variables:
;; eval: (setq live-code-talks-title-regexp "^\\s-*;;;\\s-+#\\s-+\\([^#].*\\)$")
;; eval: (setq live-code-talks-subtitle-regexp "^\\s-*;;;\\s-+##\\s-+\\([^#].*\\)$")
;; eval: (setq live-code-talks-subsubtitle-regexp "^\\s-*;;;\\s-+###\\s-+\\([^#].*\\)$")
;; eval: (setq live-code-talks-comment-regexp "^\\s-*;;;\\(\\s-*[^[ #{].*\\)$")
;; End:
